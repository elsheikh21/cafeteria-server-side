**Description:**

Java web application
Server Side: Java EE
SAP Fiori UI/UX application 
Client Side: HTML5, CSS3, JavaScript, and SAPUI5.

It is an E-Cafeteria application, where the e-money is employees' points which is inserted at the time their data inserted.
Cafeteria Order items are uploaded from an excel file as well by the user group called administrators.

Administrators set defaults points if needed and the extra points if such facility is there, 
as well as they have the authority to reset accounts password of other user groups (Captains, Cashiers),
added to creating them, not only that, they also have the authority to delete their accounts and change their roles.

Users to login, reset, change their passwords, as well as, create, delete, print orders.