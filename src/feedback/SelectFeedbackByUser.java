package feedback;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/SelectByUser")
public class SelectFeedbackByUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SelectFeedbackByUser.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private FeedbackDAO feedbackDAO;
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			feedbackDAO = new FeedbackDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			// Access-Control-Allow-Origin
			String origin = request.getHeader("Origin");
			response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
			response.setHeader("Vary", "Origin");
			// Access-Control-Max-Age
			response.setHeader("Access-Control-Max-Age", "3600");
			// Access-Control-Allow-Credentials
			response.setHeader("Access-Control-Allow-Credentials", "true");
			// Access-Control-Allow-Methods
			response.setHeader("Access-Control-Allow-Methods", "POST, GET");
			// Access-Control-Allow-Headers
			response.setHeader("Access-Control-Allow-Headers",
					"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
			response.setContentType("application/json;charset=UTF-8");

			JSONObject jsonObj = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					jsonObj.put("Feedback", doSelect(request));
				} else {
					jsonObj.put("status", "-100");
				}
			} else {
				jsonObj.put("status", "-100");
			}
			response.getWriter().write(jsonObj.toString());
		} catch (JSONException | SQLException e) {
			response.getWriter().println("Get Operation failed with reason: " + e.getMessage());
			LOGGER.error("Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private Feedback doSelect(HttpServletRequest request) throws NumberFormatException, SQLException {
		String userID = request.getParameter("U_ID");
		if (userID.length() > 0) {
			if (userID.matches("\\d+$")) {
				int userId = Integer.parseInt(userID);
				return feedbackDAO.searchByUser(userId);
			}
		}
		return null;
	}

}
