package feedback;

import java.sql.Date;
import java.sql.Time;

public class Feedback {
	private int F_ID;
	private Date F_Date;
	private Time F_Time;
	private int E_ID;
	private int U_ID;
	private int F_RATING;
	private String F_Feedback;

	public int getF_ID() {
		return F_ID;
	}

	public void setF_ID(int f_ID) {
		F_ID = f_ID;
	}

	public Date getF_Date() {
		return F_Date;
	}

	public void setF_Date(Date f_Date) {
		F_Date = f_Date;
	}

	public Time getF_Time() {
		return F_Time;
	}

	public void setF_Time(Time f_Time) {
		F_Time = f_Time;
	}

	public int getE_ID() {
		return E_ID;
	}

	public void setE_ID(int e_ID) {
		E_ID = e_ID;
	}

	public int getU_ID() {
		return U_ID;
	}

	public void setU_ID(int u_id) {
		U_ID = u_id;
	}

	public int getF_RATING() {
		return F_RATING;
	}

	public void setF_RATING(int f_RATING) {
		F_RATING = f_RATING;
	}

	public String getF_Feedback() {
		return F_Feedback;
	}

	public void setF_Feedback(String f_Feedback) {
		F_Feedback = f_Feedback;
	}

	@Override
	public String toString() {
		return "Feedback [F_ID=" + F_ID + ", F_Date=" + F_Date + ", F_Time=" + F_Time + ", E_ID=" + E_ID + ", U_ID="
				+ U_ID + ", F_RATING=" + F_RATING + ", F_Feedback=" + F_Feedback + "]";
	}

}
