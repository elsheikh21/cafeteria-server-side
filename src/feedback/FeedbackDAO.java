package feedback;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import javax.sql.DataSource;

public class FeedbackDAO {
	private DataSource dataSource;

	public FeedbackDAO(DataSource newDataSource) throws SQLException {
		setDataSource(newDataSource);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource newDataSource) throws SQLException {
		this.dataSource = newDataSource;
		checkTable();
	}

	private void checkTable() throws SQLException {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			if (!existsTable(connection)) {
				createTable(connection);
			}
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	private boolean existsTable(Connection conn) throws SQLException {
		DatabaseMetaData meta = conn.getMetaData();
		ResultSet rs = meta.getTables(null, null, "T_FEEDBACK", null);
		while (rs.next()) {
			String name = rs.getString("TABLE_NAME");
			if (name.equals("T_FEEDBACK")) {
				return true;
			}
		}
		return false;
	}

	private void createTable(Connection connection) throws SQLException {
		PreparedStatement pstmt = connection.prepareStatement("CREATE COLUMN TABLE T_FEEDBACK"
				+ "(F_ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY," + " F_DATE DATE UNIQUE NOT NULL, "
				+ "F_TIME TIME NOT NULL, " + "E_ID INTEGER NOT NULL, " + "U_ID INTEGER NOT NULL, "
				+ "F_RATING INTEGER NOT NULL, " + "F_FEEDBACK VARCHAR (255) NOT NULL)");
		pstmt.executeUpdate();
	}

	public ArrayList<Feedback> selectAll() throws SQLException {
		checkTable();
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_FEEDBACK");
			ResultSet rs = pstmt.executeQuery();
			ArrayList<Feedback> list = new ArrayList<Feedback>();
			if (rs.next()) {
				do {
					Feedback feedback = new Feedback();
					feedback.setF_ID(rs.getInt("F_ID"));
					feedback.setF_Date(rs.getDate("F_DATE"));
					feedback.setF_Time(rs.getTime("F_TIME"));
					feedback.setF_RATING(rs.getInt("f_RATING"));
					feedback.setE_ID(rs.getInt("E_ID"));
					feedback.setU_ID(rs.getInt("U_ID"));
					feedback.setF_Feedback(rs.getString("F_FEEDBACK"));
					list.add(feedback);
				} while (rs.next());
			}
			return list;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public Feedback searchByEmployee(int employeeID) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		Feedback feedback = new Feedback();
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_FEEDBACK WHERE E_ID = ?");
			pstmt.setInt(1, employeeID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				feedback.setF_ID(rs.getInt("F_ID"));
				feedback.setF_Date(rs.getDate("F_DATE"));
				feedback.setF_Time(rs.getTime("F_TIME"));
				feedback.setF_RATING(rs.getInt("F_RATING"));
				feedback.setF_Feedback(rs.getString("F_FEEDBACK"));
				feedback.setE_ID(rs.getInt("E_ID"));
			}
			return feedback;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public Feedback searchByUser(int userID) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		Feedback feedback = new Feedback();
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_FEEDBACK WHERE U_ID = ?");
			pstmt.setInt(1, userID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				feedback.setF_ID(rs.getInt("F_ID"));
				feedback.setF_Date(rs.getDate("F_DATE"));
				feedback.setF_Time(rs.getTime("F_TIME"));
				feedback.setF_RATING(rs.getInt("F_RATING"));
				feedback.setF_Feedback(rs.getString("F_FEEDBACK"));
				feedback.setE_ID(rs.getInt("E_ID"));
			}
			return feedback;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public ArrayList<Feedback> selectByDate(Date searchDate) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_FEEDBACK WHERE F_DATE >= ?");
			pstmt.setDate(1, searchDate);
			ResultSet rs = pstmt.executeQuery();
			ArrayList<Feedback> list = new ArrayList<Feedback>();
			if (rs.next()) {
				do {
					Feedback feedback = new Feedback();
					feedback.setF_ID(rs.getInt("F_ID"));
					feedback.setF_Date(rs.getDate("F_DATE"));
					feedback.setF_Time(rs.getTime("F_TIME"));
					feedback.setF_RATING(rs.getInt("f_RATING"));
					feedback.setE_ID(rs.getInt("E_ID"));
					feedback.setU_ID(rs.getInt("U_ID"));
					list.add(feedback);
				} while (rs.next());
			}
			return list;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public double getAverage() throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		double avg = 0.0;
		try {
			pstmt = connection.prepareStatement("SELECT AVG(F_RATING) AS F_AVG FROM T_FEEDBACK");
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				do {
					avg = rs.getInt("F_AVG");
				} while (rs.next());
			}
			return avg;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public String saveFeedback(String user, String rating, String feedback) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement prpstmt = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format1.setTimeZone(TimeZone.getTimeZone("Africa/Cairo")); // better
																	// than
																	// using IST
		String currentDateTime = format1.format(cal.getTime());
		String currentDate = currentDateTime.substring(0, 10);
		String currentTime = currentDateTime.substring(11);
		try {
			prpstmt = connection.prepareStatement(
					"INSERT INTO T_FEEDBACK (F_DATE, F_TIME, E_ID, U_ID, F_RATING, F_FEEDBACK) VALUES (?, ?, ?, ?, ?, ?)");
			prpstmt.setDate(1, Date.valueOf(currentDate));
			prpstmt.setTime(2, Time.valueOf(currentTime));
			prpstmt.setInt(5, Integer.valueOf(rating));
			prpstmt.setString(6, feedback);

			if (user.contains("Employee")) {
				String[] getEmpID = user.split(",");
				String[] empId = getEmpID[0].split("=");

				prpstmt.setInt(3, Integer.valueOf(empId[1].trim()));
				prpstmt.setInt(4, 0);
			} else {
				String[] getUsrID = user.split(",");
				String[] usrID = getUsrID[0].split(":");

				prpstmt.setInt(3, 0);
				prpstmt.setInt(4, Integer.valueOf(usrID[1].trim()));
			}
			return prpstmt.executeUpdate() > 0 ? "Success" : "Failure";
		} finally {
			if (prpstmt != null)
				prpstmt.close();
			if (connection != null)
				connection.close();
		}
	}
}
