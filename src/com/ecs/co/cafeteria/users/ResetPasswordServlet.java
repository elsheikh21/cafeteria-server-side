package com.ecs.co.cafeteria.users;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

//import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.cafeteria.utilities.PasswordHashing;

@WebServlet("/reset")
public class ResetPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ResetPasswordServlet.class);
	private UserDAO userDAO;
	private final List<String> allowedOrigins = Arrays
			.asList("https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			userDAO = new UserDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject jo = new JSONObject();
			jo.put("status", String.valueOf(doTokenizedReset(request)));
			// Logic flaw
			// HttpSession session = request.getSession(false);
			// if (session != null) {
			// String user = (String)
			// session.getServletContext().getAttribute("User");
			// if (user != null) {
			// jo.put("status", String.valueOf(doTokenizedReset(request)));
			// } else {
			// jo.put("status", "-100");
			// }
			// } else {
			// jo.put("status", "-100");
			// }
			response.getWriter().write(jo.toString());
		} catch (Exception e) {
			response.getWriter().println("Password change operation failed with reason: " + e);
			LOGGER.error("Password change Operation failed", e);
		}

	}

	private int doTokenizedReset(HttpServletRequest request) throws SQLException, FileNotFoundException, IOException {
		if (request.getParameter("email") == null) {
			String token = request.getParameter("token").trim();
			String newPassword = request.getParameter("newPassword").trim();
			String ConfirmNewPassword = request.getParameter("ConfirmNewPassword").trim();
			final Pattern NewPasswordREGEX = Pattern.compile("^[a-zA-Z0-9]*$", Pattern.CASE_INSENSITIVE);

			Matcher PWmatcher = NewPasswordREGEX.matcher(newPassword);
			String hashedPassword = PasswordHashing.HashPassword(newPassword, request.getServletContext());
			if (token.length() > 0 && newPassword.length() >= 15 && newPassword.equals(ConfirmNewPassword)
					&& PWmatcher.matches()) {
				String[] array = userDAO.CheckToken(token);
				if (array[0] == "88") {
					return userDAO.ChangePassword(array[1], hashedPassword, getServletContext()) == 4 ? 88 : -4;
				} else if (array[0] == "-89") {
					return -89;
				} else {
					return -88;
				}
			}
			return -88;
		} else {
			if (userDAO.doResetProcess(request.getParameter("email"), getServletContext()) == 5) {
				return 89;
			} else
				return -90;
		}
	}
}
