package com.ecs.co.cafeteria.users;

public class User {

	private int U_ID;
	private String U_FirstName;
	private String U_LastName;
	private String U_email;
	private String U_password;
	private boolean U_init;
	private String U_role;
	private String U_Token;

	public String getU_LastName() {
		return U_LastName;
	}

	public void setU_LastName(String u_LastName) {
		U_LastName = u_LastName;
	}

	public String getU_FirstName() {
		return U_FirstName;
	}

	public void setU_FirstName(String u_FirstName) {
		U_FirstName = u_FirstName;
	}

	public String getU_email() {
		return U_email;
	}

	public void setU_email(String u_email) {
		U_email = u_email;
	}

	public int getU_ID() {
		return U_ID;
	}

	public void setU_ID(int u_ID) {
		U_ID = u_ID;
	}

	public String getU_password() {
		return U_password;
	}

	public void setU_password(String u_password) {
		U_password = u_password;
	}

	public boolean getU_init() {
		return U_init;
	}

	public void setU_init(boolean u_init) {
		U_init = u_init;
	}

	public String getU_role() {
		return U_role;
	}

	public void setU_role(String u_role) {
		U_role = u_role;
	}

	public String getU_Token() {
		return U_Token;
	}

	public void setU_Token(String u_Token) {
		U_Token = u_Token;
	}

	@Override
	public String toString() {
		return "ID: " + U_ID + ", Name: " + U_FirstName + " " + U_LastName + ", email: " + U_email + ", role: "
				+ U_role;
	}
}
