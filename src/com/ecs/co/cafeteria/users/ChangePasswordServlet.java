package com.ecs.co.cafeteria.users;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/change")
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ChangePasswordServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private UserDAO userDAO;
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			userDAO = new UserDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		JSONObject jo = new JSONObject();
		try {
			jo.put("status", String.valueOf(doChange(request)));
			response.getWriter().write(jo.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Password change operation failed with reason: " + e);
			LOGGER.error("Password change Operation failed", e);
		}
	}

	private int doChange(HttpServletRequest request) throws SQLException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String NewPassword = request.getParameter("newpassword");
		String ConfirmPassword = request.getParameter("confirmpassword");

		final Pattern emailAddressREGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		final Pattern NewPasswordREGEX = Pattern
				.compile("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{15,}", Pattern.CASE_INSENSITIVE);

		Matcher PWmatcher = NewPasswordREGEX.matcher(NewPassword);
		Matcher Ematcher = emailAddressREGEX.matcher(email);

		if (email != null && password != null && !email.trim().isEmpty() && !password.trim().isEmpty()
				&& Ematcher.matches() && NewPassword != null && !NewPassword.trim().isEmpty() && ConfirmPassword != null
				&& !ConfirmPassword.trim().isEmpty() && PWmatcher.matches() && NewPassword.equals(ConfirmPassword)) {
			User user = new User();
			user.setU_email(email.trim());
			user.setU_password(password.trim());
			return userDAO.ChangeUserPassword(user, NewPassword, request.getServletContext());
		}
		return -4;
	}

}
// private void appendChangeFailure(HttpServletResponse response) {
// try {
// response.getWriter().println("<p>" + "You failed to change your password." +
// "</p>");
// } catch (IOException e) {
// e.printStackTrace();
// }
// }
//
// private void appendChangeSuccess(HttpServletResponse response) {
// try {
// response.getWriter().println("<p>" + "Password is changed." + "</p>");
// } catch (IOException e) {
// e.printStackTrace();
// }
// }