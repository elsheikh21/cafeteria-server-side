package com.ecs.co.cafeteria.users;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;
//import javax.servlet.http.Cookie;
import javax.servlet.ServletConfig;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.json.JSONObject;
import org.json.JSONException;
import org.slf4j.LoggerFactory;

@WebServlet("/adminreset")
public class AdminResetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminResetServlet.class);
	private UserDAO userDAO;
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			userDAO = new UserDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");

		response.setContentType("application/json;charset=UTF-8");
		JSONObject jo = new JSONObject();
		try {
			// Approach #01
			// ----------------------------------------------------------------------
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					jo.put("status", String.valueOf(doAdminReset(request)));
				} else {
					jo.put("status", "-100");
				}
			} else {
				jo.put("status", "-100");
			}
			// Approach #02
			// ----------------------------------------------------------------------
			// Cookie[] cookies = request.getCookies();
			// if (cookies != null) {
			// for (Cookie cookie : cookies) {
			// if (cookie.getName().equals("User_Role")) {
			// String str = cookie.getValue();
			// System.out.println(str);
			// jo.put("status", String.valueOf(doAdminReset(request)));
			// }
			// }
			// } else {
			// jo.put("status", "-100");
			//
			// }
			// response.getWriter().write(jo.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Password change operation failed with reason: " + e);
			LOGGER.error("Password change Operation failed", e);
		}
		response.getWriter().write(jo.toString());
		// doGet(request, response);
	}

	private int doAdminReset(HttpServletRequest request) throws SQLException {
		String email = request.getParameter("email");

		final Pattern emailAddressREGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		Matcher Ematcher = emailAddressREGEX.matcher(email);

		if (email != null && !email.trim().isEmpty() && Ematcher.matches()) {
			User user = new User();
			user.setU_email(email.trim());
			return userDAO.AdminResetUserPassword(user, request.getServletContext());
		}
		return -7;
	}

}
