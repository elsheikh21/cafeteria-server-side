package com.ecs.co.cafeteria.users;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.cafeteria.utilities.PasswordHashing;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8919617-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private UserDAO userDAO;
	private DataSource ds;
	private Cookie cookie;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			userDAO = new UserDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");

		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject jo = new JSONObject();
			jo.put("status", String.valueOf(doLogin(request)));
			response.getWriter().write(jo.toString());
			// response.addCookie(cookie);
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Post Operation failed with reason: " + e.getMessage());
			LOGGER.error("Operation failed", e);
		}
	}

	private int doLogin(HttpServletRequest request) throws ServletException, IOException, SQLException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String hashedPassword = PasswordHashing.HashPassword(password, request.getServletContext());
		final Pattern emailAddressREGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = emailAddressREGEX.matcher(email);
		if (email != null && password != null && !email.trim().isEmpty() && !password.trim().isEmpty()
				&& matcher.matches()) {
			User user = new User();
			user.setU_email(email.trim());
			user.setU_password(hashedPassword.trim());
			int UserCode = userDAO.LoginUser(user);
			if (UserCode >= 0) {
				StartSession(request);
			}
			return UserCode;
		}
		return -2;
	}

	private void StartSession(HttpServletRequest request) throws SQLException {
		HttpSession session = request.getSession(true);
		if (!session.isNew()) {
			session.invalidate();
			session = request.getSession(true);
		}
		session.setMaxInactiveInterval(15 * 60);
		User UserObj = userDAO.GetUser(request.getParameter("email"));
		String user = UserObj.toString();
		session.getServletContext().setAttribute("User", user);

		String user_role = UserObj.getU_role().toString();
		cookie = new Cookie("User_Role", user_role);
		cookie.setPath(request.getContextPath());
		cookie.setHttpOnly(false);
		cookie.setPath("/");
		cookie.setMaxAge(-1);
	}

}

// int loginAttempt = (Integer)session.getAttribute("loginCount");
// if (loginAttempt > 3 ){
// // Error message/page redirection
// }else{
// session.setAttribute("loginCount",loginAttempt++);
// }