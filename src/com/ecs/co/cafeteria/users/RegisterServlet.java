package com.ecs.co.cafeteria.users;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(RegisterServlet.class);
	private UserDAO userDAO;
	private final List<String> allowedOrigins = Arrays
			.asList("https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			config = getServletConfig();
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			userDAO = new UserDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		JSONObject jo = new JSONObject();
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					jo.put("status", String.valueOf(doAdd(request)));
				} else {
					jo.put("status", "-100");
				}
			} else {
				jo.put("status", "-100");
			}
			response.getWriter().write(jo.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Post Operation failed with reason: " + e.getMessage());
			LOGGER.error("Operation failed", e);
		}
	}

	private int doAdd(HttpServletRequest request) throws ServletException, IOException, SQLException {
		String email = request.getParameter("email");
		String role = request.getParameter("role");
		String generate = request.getParameter("generate");
		String fName = request.getParameter("fName");
		String lName = request.getParameter("lName");

		final Pattern emailAddressREGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		final Pattern nameREGEX = Pattern.compile("^[A-Za-z]*$", Pattern.CASE_INSENSITIVE);

		Matcher matcher = emailAddressREGEX.matcher(email);
		Matcher matcherLastName = nameREGEX.matcher(lName);
		Matcher matcherFirstName = nameREGEX.matcher(fName);

		if (email != null && role != null && !email.trim().isEmpty() && !role.trim().isEmpty()) {
			if ((role.equalsIgnoreCase("Captain") || role.equalsIgnoreCase("Cashier") || role.equalsIgnoreCase("Admin"))
					&& matcher.matches()) {
				if (fName != null && lName != null && !fName.trim().isEmpty() && !lName.trim().isEmpty()
						&& matcherLastName.matches() && matcherFirstName.matches()) {
					User user = new User();
					user.setU_email(email.trim());
					user.setU_role(role.trim());
					user.setU_FirstName(fName.trim());
					user.setU_LastName(lName.trim());
					return userDAO.RegisterUser(user, request.getServletContext(), generate);
				}
			}
		}
		return -6;
	}
}

/*
 * In doPost's method HttpSession session = request.getSession(false); if
 * (session != null) { String user = (String)
 * session.getServletContext().getAttribute("User"); // User user = (User)
 * session.getAttribute("User"); if (user != null) { jo.put("status",
 * String.valueOf(doAdd(request))); } else { jo.put("status", "-100"); } } else
 * { jo.put("status", "-100"); }
 */

// private void appendUserTable(HttpServletResponse response) throws
// SQLException, IOException {
// List<User> resultList = userDAO.selectAllUsers();
// response.getWriter()
// .println("<p><table border=\"1\"><tr><th colspan=\"3\">" + resultList.size()
// + " entries in the Database</th></tr>"
// + (resultList.isEmpty() ? "<tr><td colspan=\"3\">Database is empty</td></tr>"
// : "<tr><th>ID</th><th>Role</th><th>Email</th></tr>"));
// IXSSEncoder xssEncoder = XSSEncoder.getInstance();
// for (User u : resultList) {
// response.getWriter().println("<tr><td>" +
// xssEncoder.encodeHTML(String.valueOf(u.getU_ID())) + "</td><td>"
// + xssEncoder.encodeHTML(u.getU_role()) + "</td><td>" + u.getU_email() +
// "</td></tr>");
// }
// response.getWriter().println("</table></p>");
// }
//
// private void appendAddForm(HttpServletResponse response) throws IOException {
// response.getWriter()
// .println("<p><form action=\"\" method=\"post\">" + "Email:<input
// type=\"text\" name=\"email\">"
// + "&nbsp;Role:<input type=\"text\" name=\"role\">"
// + "&nbsp;<input type=\"submit\" value=\"Register User\">" + "</form></p>");
// }
// private void appendAddSuccess(HttpServletResponse response) {
// try {
// response.getWriter().println("<p>" + "You have registered the new user."
// + "</p>");
// } catch (IOException e) {
// e.printStackTrace();
// }
// }
//
// private void appendAddFailure(HttpServletResponse response) {
// try {
// response.getWriter().println("<p>" + "You failed to register the new
// user." + "</p>");
// } catch (IOException e) {
// e.printStackTrace();
// }
// }
// import com.sap.security.core.server.csi.IXSSEncoder;
// import com.sap.security.core.server.csi.XSSEncoder;
// import java.util.List;