package com.ecs.co.cafeteria.points;

public class Points {
	private int defaultPoints;
	private int extraPoints;

	public Points() {
		super();
	}

	public int getDefaultPoints() {
		return defaultPoints;
	}

	public void setDefaultPoints(int defaultPoints) {
		this.defaultPoints = defaultPoints;
	}

	public int getExtraPoints() {
		return extraPoints;
	}

	public void setExtraPoints(int extraPoints) {
		this.extraPoints = extraPoints;
	}
	
}
