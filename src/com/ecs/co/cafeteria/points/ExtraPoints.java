package com.ecs.co.cafeteria.points;

public class ExtraPoints {
	private int EP_ID;
	private int employeeID;
	private String addedBy;
	private String dateTime;
	private int extraPoints;
	private String grantedTo;
	
	public ExtraPoints() {
		super();
	}

	public int getEP_ID() {
		return EP_ID;
	}

	public void setEP_ID(int eP_ID) {
		EP_ID = eP_ID;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public int getExtraPoints() {
		return extraPoints;
	}

	public void setExtraPoints(int extraPoints) {
		this.extraPoints = extraPoints;
	}

	public String getGrantedTo() {
		return grantedTo;
	}

	public void setGrantedTo(String grantedTo) {
		this.grantedTo = grantedTo;
	}

}
