package com.ecs.co.cafeteria.points;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

public class PointsDAO {
	private DataSource dataSource;

	public PointsDAO(DataSource newDataSource) throws SQLException {
		setDataSource(newDataSource);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource newDataSource) throws SQLException {
		this.dataSource = newDataSource;
		checkTable();
	}

	private void checkTable() throws SQLException {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			if (!existsPointsTable(connection)) {
				createPointsTable(connection);
			}
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	private boolean existsPointsTable(Connection conn) throws SQLException {
		DatabaseMetaData meta = conn.getMetaData();
		ResultSet rs = meta.getTables(null, null, "T_POINTS", null);
		while (rs.next()) {
			String name = rs.getString("TABLE_NAME");
			if (name.equals("T_POINTS")) {
				return true;
			}
		}
		return false;
	}

	private void createPointsTable(Connection connection) throws SQLException {
		PreparedStatement pstmt = connection.prepareStatement(
				"CREATE COLUMN TABLE T_POINTS " + "(P_ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, "
						+ "P_DEFAULT_POINTS INTEGER NOT NULL, " + "P_EXTRA_POINTS INTEGER NOT NULL)");
		pstmt.executeUpdate();
		FirstEntries();
	}

	private void FirstEntries() throws SQLException {
		checkTable();
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection
					.prepareStatement("INSERT INTO T_POINTS (P_DEFAULT_POINTS, P_EXTRA_POINTS) VALUES (0, 0)");
			pstmt.executeUpdate();
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (connection != null)
				connection.close();
		}
	}

	public ArrayList<Points> showPoints() throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_POINTS");
			ResultSet rs = pstmt.executeQuery();
			ArrayList<Points> list = new ArrayList<Points>();
			if (rs.next()) {
				do {
					Points point = new Points();
					point.setDefaultPoints(rs.getInt("P_DEFAULT_POINTS"));
					point.setExtraPoints(rs.getInt("P_EXTRA_POINTS"));
					list.add(point);
				} while (rs.next());
			}
			return list;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public boolean setDefaultPoints(int defaultPoints) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("UPDATE T_POINTS SET P_DEFAULT_POINTS = ?");
			pstmt.setInt(1, defaultPoints);
			return (pstmt.executeUpdate() > 0) ? true : false;
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (connection != null)
				connection.close();
		}
	}

	public boolean setExtraPoints(int extraPoints) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("UPDATE T_POINTS SET P_EXTRA_POINTS = ?");
			pstmt.setInt(1, extraPoints);
			return (pstmt.executeUpdate() > 0) ? true : false;
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (connection != null)
				connection.close();
		}
	}

	public boolean setDefaultsForEmployees(int defaultPoints) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("UPDATE T_EMPLOYEES SET E_POINTS = ?");
			pstmt.setInt(1, defaultPoints);
			return (pstmt.executeUpdate() > 0) ? true : false;
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (connection != null)
				connection.close();
		}
	}

}
