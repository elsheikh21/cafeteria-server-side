package com.ecs.co.cafeteria.orders;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.cafeteria.items.Items;

import beverages.Beverages;

@WebServlet("/selectorder")
public class SelectOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SelectOrderServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private DataSource ds;
	private OrdersDAO ordersDAO;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			ordersDAO = new OrdersDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject order = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					JSONObject jo = new JSONObject();
					Order o = doSelect(request);
					JSONArray array = new JSONArray();
					ArrayList<Items> orderItems = o.getO_Items();
					if (orderItems != null)
						if (orderItems.size() > 0)
							for (int i = 0; i < orderItems.size(); i++) {
								Items item = orderItems.get(i);
								JSONObject orderItemsJSONObj = new JSONObject();
								orderItemsJSONObj.put("OrderItemCategory", item.getI_Category());
								orderItemsJSONObj.put("OrderItemQty", item.getI_Quantity());
								array.put(orderItemsJSONObj);
							}
					ArrayList<Beverages> orderBeverages = o.getO_Beverages();
					if (orderBeverages != null)
						if (orderBeverages.size() > 0)
							for (int i = 0; i < orderBeverages.size(); i++) {
								Beverages beverage = orderBeverages.get(i);
								JSONObject orderBeveragesJSONObj = new JSONObject();
								orderBeveragesJSONObj.put("OrderItemCategory", beverage.getB_Category());
								orderBeveragesJSONObj.put("OrderItemQty", beverage.getB_Quantity());
								array.put(orderBeveragesJSONObj);
							}
					jo.put("OrderItems", array);
					jo.put("OrderID", o.getO_ID());
					jo.put("EmployeeID", o.getE_ID());
					jo.put("OrderDate", o.getO_Date());
					jo.put("OrderPoints", o.getO_Points());
					order.put("Order", jo);
				} else {
					order.put("status", "-100");
				}
			} else {
				order.put("status", "-100");
			}
			response.getWriter().write(order.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Selection operation failed with reason: " + e);
			LOGGER.error("Selection Operation failed", e);
		}
	}

	private Order doSelect(HttpServletRequest request) throws NumberFormatException, SQLException {
		String orderID = request.getParameter("orderID");
		if (orderID != null) {
			if (orderID.matches("[0-9]+")) {
				return ordersDAO.selectOrder(Integer.valueOf(orderID));
			}
		}
		return null;
	}

}
