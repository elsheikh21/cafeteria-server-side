package com.ecs.co.cafeteria.orders;

import java.io.IOException;
import java.sql.SQLException;
//import java.sql.Time;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.cafeteria.items.Items;

import beverages.Beverages;

@WebServlet("/selectdateorder")
public class SelectOrderDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SelectOrderDateServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private OrdersDAO ordersDAO;
	private DataSource ds;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			ordersDAO = new OrdersDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject ordersJsonObj = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					ArrayList<Order> ordersArrList = doSelect(request);
					JSONArray jsonArr = new JSONArray();
					for (int i = 0; i < ordersArrList.size(); i++) {
						JSONObject Order = new JSONObject();
						Order order = ordersArrList.get(i);
						JSONArray itemsJSonArr = new JSONArray();
						ArrayList<Items> OrderItems = order.getO_Items();
						if (OrderItems != null)
							if (OrderItems.size() > 0)
								for (int j = 0; j < OrderItems.size(); j++) {
									JSONObject jo = new JSONObject();
									Items item = OrderItems.get(j);
									jo.put("OrderItemCategory", item.getI_Category());
									jo.put("OrderItemQty", item.getI_Quantity());
									itemsJSonArr.put(jo);
								}
						ArrayList<Beverages> OrderBeverages = order.getO_Beverages();
						if (OrderBeverages != null)
							if (OrderBeverages.size() > 0)
								for (int j = 0; j < OrderBeverages.size(); j++) {
									JSONObject jo = new JSONObject();
									Beverages beverages = OrderBeverages.get(j);
									jo.put("OrderItemCategory", beverages.getB_Category());
									jo.put("OrderItemQty", beverages.getB_Quantity());
									itemsJSonArr.put(jo);
								}
						Order.put("OrderID", order.getO_ID());
						Order.put("OrderDate", order.getO_Date());
						Order.put("OrderItems", itemsJSonArr);
						Order.put("OrderPoints", order.getO_Points());
						jsonArr.put(Order);
					}
					ordersJsonObj.put("Orders", jsonArr);
				} else {
					ordersJsonObj.put("status", "-100");
				}
			} else {
				ordersJsonObj.put("status", "-100");
			}
			response.getWriter().write(ordersJsonObj.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Selection operation failed with reason: " + e);
			LOGGER.error("Selection Operation failed", e);
		}
	}

	private ArrayList<Order> doSelect(HttpServletRequest request) throws NumberFormatException, SQLException {
		String orderDateFrom = request.getParameter("dateFrom");
		// String orderTimeFrom = request.getParameter("timeFrom");
		String orderDateTo = request.getParameter("dateTo");
		// String orderTimeTo = request.getParameter("timeTo");

		String datePattern = "\\d{4}-\\d{2}-\\d{2}";
		// String timePattern =
		// "(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)+";

		// Case A: User selected only from date & from time
		if (orderDateFrom != null && orderDateTo == null) {
			if (!orderDateFrom.isEmpty() && !"".equals(orderDateFrom) && !(orderDateFrom.trim().length() > 0)) {
				if (orderDateFrom.matches(datePattern)) {
					return ordersDAO.selectOrdersFromDate(Date.valueOf(orderDateFrom));
				}
			}
		}

		// Case B: User selected only to date & to time
		else if (orderDateTo != null && orderDateFrom == null) {
			if (!orderDateTo.isEmpty() && !"".equals(orderDateTo) && !(orderDateTo.trim().length() > 0)) {
				if (orderDateTo.matches(datePattern)) {
					return ordersDAO.selectOrdersToDate(Date.valueOf(orderDateTo));
				}
			}
		}

		// Case C: User selected FROM as well as TO
		else if (orderDateTo != null && orderDateFrom != null) {
			if (!orderDateTo.isEmpty() && !"".equals(orderDateTo) && (orderDateTo.trim().length() > 0)
					&& !orderDateFrom.isEmpty() && !"".equals(orderDateFrom) && (orderDateFrom.trim().length() > 0)) {
				if (orderDateTo.matches(datePattern) && orderDateFrom.matches(datePattern)) {
					if(orderDateTo.equals(orderDateFrom))
						return ordersDAO.selectOrdersOn(Date.valueOf(orderDateFrom));
					else 
						return ordersDAO.selectOrdersFromToDate(Date.valueOf(orderDateFrom), Date.valueOf(orderDateTo));
				}
			}else if (!orderDateFrom.isEmpty() && !"".equals(orderDateFrom) && (orderDateFrom.trim().length() > 0)) {
				if (orderDateFrom.matches(datePattern)) {
					return ordersDAO.selectOrdersFromDate(Date.valueOf(orderDateFrom));
				}
			} else if (!orderDateTo.isEmpty() && !"".equals(orderDateTo) && (orderDateTo.trim().length() > 0)) {
				if (orderDateTo.matches(datePattern)) {
					return ordersDAO.selectOrdersToDate(Date.valueOf(orderDateTo));
				}
			}
			
		}

		return new ArrayList<Order>();
	}
}

/*
 * && orderTimeTo == null && orderTimeFrom == null else if (orderTimeFrom !=
 * null && orderDateFrom != null && orderDateTo == null && orderTimeTo == null)
 * { if (orderDateFrom.matches(datePattern) &&
 * orderTimeFrom.matches(timePattern)) { return
 * ordersDAO.selectOrdersFromDateAndTime(Date.valueOf(orderDateFrom),
 * Time.valueOf(orderTimeFrom)); } }
 */
// End of Case A

/*
 * && && orderTimeTo == null orderTimeFrom == null else if (orderDateTo != null
 * && orderTimeTo != null && orderDateFrom == null && orderTimeFrom == null) {
 * if (orderDateTo.matches(datePattern) && orderTimeTo.matches(timePattern)) {
 * return ordersDAO.selectOrdersToDateAndTime(Date.valueOf(orderDateTo),
 * Time.valueOf(orderTimeTo)); } }
 */
// End of Case B
/*
 * && orderTimeFrom == null&& orderTimeTo == null else if (orderDateTo != null
 * && orderTimeTo != null && orderDateFrom != null && orderTimeFrom != null) {
 * if (orderDateTo.matches(datePattern) && orderTimeTo.matches(timePattern)) {
 * return ordersDAO.selectOrdersFromToDateAndTime(Date.valueOf(orderDateFrom),
 * Time.valueOf(orderTimeFrom), Date.valueOf(orderDateTo),
 * Time.valueOf(orderTimeTo)); } }
 */
// End of Case C
/*
 * else if (orderTimeFrom != null && orderDateFrom == null && (orderDateTo ==
 * null && orderTimeTo == null)) { if
 * (orderTimeFrom.matches("(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)+"
 * )) { return ordersDAO.selectOrdersFromTime(Time.valueOf(orderTimeFrom)); } }
 */