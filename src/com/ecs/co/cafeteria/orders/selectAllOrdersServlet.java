package com.ecs.co.cafeteria.orders;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.cafeteria.items.Items;

import beverages.Beverages;

@WebServlet("/selectAllOrders")
public class selectAllOrdersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SelectOrderServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private DataSource ds;
	private OrdersDAO ordersDAO;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			ordersDAO = new OrdersDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject ordersJsonObj = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					ArrayList<Order> ordersArrList = doSelect(request);
					if (ordersArrList == null) {
						ordersJsonObj.put("status", "Failure");
					} else {
						JSONArray jsonArr = new JSONArray();
						for (int i = 0; i < ordersArrList.size(); i++) {
							JSONObject Order = new JSONObject();
							Order order = ordersArrList.get(i);
							JSONArray itemsJSonArr = new JSONArray();
							ArrayList<Items> OrderItems = order.getO_Items();
							if (OrderItems != null)
								if (OrderItems.size() > 0)
									for (int j = 0; j < OrderItems.size(); j++) {
										JSONObject jo = new JSONObject();
										Items item = OrderItems.get(j);
										jo.put("OrderItemCategory", item.getI_Category());
										jo.put("OrderItemQty", item.getI_Quantity());
										itemsJSonArr.put(jo);
									}
							ArrayList<Beverages> OrderBeverages = order.getO_Beverages();
							if (OrderBeverages != null)
								if (OrderBeverages.size() > 0)
									for (int j = 0; j < OrderBeverages.size(); j++) {
										JSONObject jo = new JSONObject();
										Beverages beverages = OrderBeverages.get(j);
										jo.put("OrderItemCategory", beverages.getB_Category());
										jo.put("OrderItemQty", beverages.getB_Quantity());
										itemsJSonArr.put(jo);
									}
							Order.put("EmployeeID", order.getE_ID());
							Order.put("OrderID", order.getO_ID());
							Order.put("OrderDate", order.getO_Date());
							Order.put("OrderItems", itemsJSonArr);
							Order.put("OrderPoints", order.getO_Points());
							jsonArr.put(Order);
						}
						ordersJsonObj.put("Orders", jsonArr);
					}
				} else {
					ordersJsonObj.put("status", "-100");
				}
			} else {
				ordersJsonObj.put("status", "-100");
			}
			response.getWriter().write(ordersJsonObj.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Operation failed with reason: " + e);
			LOGGER.error("Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doGet(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	private ArrayList<Order> doSelect(HttpServletRequest request) throws NumberFormatException, SQLException {
		String orderID = request.getParameter("orderID");
		if (orderID != null)
			if (orderID.matches("[0-9]+")) {
				int oID = Integer.parseInt(orderID);
				if (oID == 0)
					return ordersDAO.selectTop(100);
			}
		String employeeID = request.getParameter("employeeID");
		if (employeeID != null)
			if (employeeID.matches("[0-9]+")) {
				int empID = Integer.parseInt(employeeID);
				if (empID == 0)
					return ordersDAO.selectTop(100);
			}
		return null;
	}

}
// if("0".equals(orderID))
// return ordersDAO.selectTop(100);