package com.ecs.co.cafeteria.orders;

import java.util.ArrayList;

import com.ecs.co.cafeteria.items.Items;

import beverages.Beverages;

public class Order {

	private int O_ID;
	private String O_Date;
	private String O_Time;
	private String O_Type;
	private int O_Points;
	private int E_ID;
	private ArrayList<Items> O_Items;
	private ArrayList<Beverages> O_Beverages;
	private String O_Ordered_By;

	public Order() {
		O_Items = new ArrayList<Items>();
	}

	public int getO_ID() {
		return O_ID;
	}

	public void setO_ID(int o_ID) {
		O_ID = o_ID;
	}

	public String getO_Date() {
		return O_Date;
	}

	public void setO_Date(String o_Date) {
		O_Date = o_Date;
	}
	
	public String getO_Time() {
		return O_Time;
	}

	public void setO_Time(String o_Time) {
		O_Time = o_Time;
	}

	public String getO_Type() {
		return O_Type;
	}

	public void setO_Type(String o_Type) {
		O_Type = o_Type;
	}

	public int getO_Points() {
		return O_Points;
	}

	public void setO_Points(int o_Points) {
		O_Points = o_Points;
	}

	public int getE_ID() {
		return E_ID;
	}

	public void setE_ID(int e_ID) {
		E_ID = e_ID;
	}

	public ArrayList<Items> getO_Items() {
		return O_Items;
	}

	public void setO_Items(ArrayList<Items> o_Items) {
		O_Items = o_Items;
	}

	public String getO_Ordered_By() {
		return O_Ordered_By;
	}

	public ArrayList<Beverages> getO_Beverages() {
		return O_Beverages;
	}

	public void setO_Beverages(ArrayList<Beverages> o_Beverages) {
		O_Beverages = o_Beverages;
	}

	public void setO_Ordered_By(String o_Ordered_By) {
		O_Ordered_By = o_Ordered_By;
	}

}
