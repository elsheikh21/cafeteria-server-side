package com.ecs.co.cafeteria.orders;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecs.co.cafeteria.items.Items;

import beverages.Beverages;

@WebServlet("/saveorder")
public class SaveOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveOrderServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private DataSource ds;
	private OrdersDAO ordersDAO;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			ordersDAO = new OrdersDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		try {
			JSONObject jo = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					jo.put("status", String.valueOf(doSave(request, user)));
				} else {
					jo.put("status", "-100");
				}
			} else {
				jo.put("status", "-100");
			}
			response.getWriter().write(jo.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Selection operation failed with reason: " + e);
			LOGGER.error("Selection Operation failed", e);
		}
	}

	private int doSave(HttpServletRequest request, String user)
			throws NumberFormatException, SQLException, JSONException {
		String employeeID = request.getParameter("employeeID");
		String orderItems = request.getParameter("orderItems");
		String orderBeverages = request.getParameter("orderBeverages");

		if (employeeID != null) {
			if (orderItems != null && orderBeverages == null) {
				if (!employeeID.isEmpty() && (employeeID.matches("[0-9]+"))) {
					if (!orderItems.isEmpty()) {
						int empID = Integer.valueOf(employeeID);
						JSONArray jsonArr = new JSONArray(orderItems);
						ArrayList<Items> itemsOrder = new ArrayList<Items>();
						for (int i = 0; i < jsonArr.length(); i++) {
							JSONObject jsonItemObj = jsonArr.getJSONObject(i);
							Items item = new Items();
							if (jsonItemObj.getInt("i_Quantity") == 0) {
								continue;
							}
							item.setI_ID(jsonItemObj.getInt("i_ID"));
							item.setI_Quantity(jsonItemObj.getInt("i_Quantity"));
							item.setI_Category_ID(jsonItemObj.getString("i_Category_ID"));
							item.setI_Category(jsonItemObj.getString("i_Category"));
							item.setI_Points(jsonItemObj.getInt("i_Points"));
							itemsOrder.add(item);
						}
						if (!itemsOrder.isEmpty()) {
							Order order = new Order();
							order.setO_Items(itemsOrder);
							order.setE_ID(empID);
							order.setO_Type("Food");
							return ordersDAO.saveEmployeeOrder(order, user);
						}
					}
				}
			} else if (orderBeverages != null && orderItems == null) {
				if (!employeeID.isEmpty() && (employeeID.matches("[0-9]+"))) {
					if (!orderBeverages.isEmpty()) {
						int empID = Integer.valueOf(employeeID);
						JSONArray jsonArr = new JSONArray(orderBeverages);
						ArrayList<Beverages> beveragesOrder = new ArrayList<Beverages>();
						for (int i = 0; i < jsonArr.length(); i++) {
							JSONObject jsonBeveragesObj = jsonArr.getJSONObject(i);
							Beverages beverage = new Beverages();
							if (jsonBeveragesObj.getInt("b_Quantity") == 0) {
								continue;
							}
							beverage.setB_ID(jsonBeveragesObj.getInt("b_ID"));
							beverage.setB_Quantity(jsonBeveragesObj.getInt("b_Quantity"));
							beverage.setB_Category_ID(jsonBeveragesObj.getString("b_Category_ID"));
							beverage.setB_Category(jsonBeveragesObj.getString("b_Category"));
							beverage.setB_Points(jsonBeveragesObj.getInt("b_Points"));
							beveragesOrder.add(beverage);
						}
						if (!beveragesOrder.isEmpty()) {
							Order order = new Order();
							order.setO_Beverages(beveragesOrder);
							order.setE_ID(empID);
							order.setO_Type("Beverages");
							// ServletContext servletContext =
							// request.getServletContext();
							return ordersDAO.saveEmployeeOrder(order, user);
						}
					}
				}
			}
		}
		return -15;
	}
}