package com.ecs.co.cafeteria.orderDetails;

public class OrderItem {

	private int I_ID;
	private int O_ID;
	private int O_Demand;
	private String I_Name;

	public int getI_ID() {
		return I_ID;
	}

	public void setI_ID(int i_ID) {
		I_ID = i_ID;
	}

	public int getO_ID() {
		return O_ID;
	}

	public void setO_ID(int o_ID) {
		O_ID = o_ID;
	}

	public int getO_Demand() {
		return O_Demand;
	}

	public void setO_Demand(int o_Demand) {
		O_Demand = o_Demand;
	}

	public String getI_Name() {
		return I_Name;
	}

	public void setI_Name(String i_Name) {
		I_Name = i_Name;
	}

}
