package com.ecs.co.cafeteria.items;

public class Items {

	private int I_ID;
	private String I_Category;
	private int I_Points;
	private int I_Quantity;
	private String I_Category_ID;
	private int I_Demand;

	public int getI_ID() {
		return I_ID;
	}

	public void setI_ID(int i_ID) {
		I_ID = i_ID;
	}

	public String getI_Category() {
		return I_Category;
	}

	public void setI_Category(String i_Category) {
		I_Category = i_Category;
	}

	public int getI_Points() {
		return I_Points;
	}

	public void setI_Points(int i_Points) {
		I_Points = i_Points;
	}

	public int getI_Quantity() {
		return I_Quantity;
	}

	public void setI_Quantity(int i_Quantity) {
		I_Quantity = i_Quantity;
	}

	public String getI_Category_ID() {
		return I_Category_ID;
	}

	public void setI_Category_ID(String i_Category_ID) {
		I_Category_ID = i_Category_ID;
	}

	public int getI_Demand() {
		return I_Demand;
	}

	public void setI_Demand(int i_Demand) {
		I_Demand = i_Demand;
	}

}
