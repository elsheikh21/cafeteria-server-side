package com.ecs.co.cafeteria.utilities;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTs {
	public static String parseJWT(String jwt) throws UnsupportedEncodingException {
		Jws<Claims> claims = Jwts.parser().setSigningKey("secret".getBytes("UTF-8")).parseClaimsJws(jwt);
		String email = claims.getBody().get("email").toString();
		// String role = claims.getBody().get("role").toString();
		return email;
	}

	public static String createJWT(String email) throws UnsupportedEncodingException {
		int hoursToms = (int) TimeUnit.HOURS.toMillis(24);
		return Jwts.builder().setSubject("users/TzMUocMF4p")
				.setExpiration(new Date(System.currentTimeMillis() + hoursToms)).claim("email", email)
				.signWith(SignatureAlgorithm.HS256, "secret".getBytes("UTF-8")).compact();
		// add .claim("role", role) to encode this to your jwt string
	}
}
