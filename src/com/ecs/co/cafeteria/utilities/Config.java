package com.ecs.co.cafeteria.utilities;

import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;

public class Config {

	Properties configFile;

	public Config(ServletContext servletContext) {
		configFile = new Properties();
		try {
			InputStream in = servletContext.getResourceAsStream("/WEB-INF/app.properties");
			configFile.load(in);
		} catch (Exception eta) {
			eta.printStackTrace();
		}
	}

	public String getProperty(String key) {
		return this.configFile.getProperty(key);
	}
}
