package com.ecs.co.cafeteria.utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.ServletContext;

public class SendMail {
	public static int send(String to, String sub, String msg, ServletContext servletContext) {
		Config cfg = new Config(servletContext);
		final String user = cfg.getProperty("user");
		final String pass = cfg.getProperty("pass");

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		});

		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(sub);
			message.setHeader("Content-Type", "text/html; charset=UTF-8");
			Multipart multipart = new MimeMultipart("alternative");
			MimeBodyPart textPart = new MimeBodyPart();

			if (sub.contains("magic")) {
				InputStream is = servletContext.getResourceAsStream("ResetPasswordTemplate.html");
				BufferedReader bf = new BufferedReader(new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line = "";
				while ((line = bf.readLine()) != null) {
					sb.append(line);
				}
				String str = sb.toString();
				str = str.replace("{{TOKEN_KEY}}", msg);
				textPart.setText(str, "UTF-8");
				multipart.addBodyPart(textPart);
				message.setContent(str, "text/html");
			} else if (sub.contains("welcomes")) {
				InputStream is = servletContext.getResourceAsStream("WelcomeEmailTemplate.html");
				BufferedReader bf = new BufferedReader(new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line = "";
				while ((line = bf.readLine()) != null) {
					sb.append(line);
				}
				String str = sb.toString();
				str = str.replace("{{PASSWORD}}", msg);
				textPart.setText(str, "UTF-8");
				multipart.addBodyPart(textPart);
				message.setContent(str, "text/html");
			} else if (sub.contains("role")) {
				InputStream is = servletContext.getResourceAsStream("ChangeRoleTemplate.html");
				BufferedReader bf = new BufferedReader(new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line = "";
				while ((line = bf.readLine()) != null) {
					sb.append(line);
				}
				String str = sb.toString();
				msg = msg.substring(msg.lastIndexOf(" ")).trim();
				str = str.replace("{{NEW_ROLE}}", msg);
				textPart.setText(str, "UTF-8");
				multipart.addBodyPart(textPart);
				message.setContent(str, "text/html");
			} else {
				InputStream is = servletContext.getResourceAsStream("ChangePasswordTemplate.html");
				BufferedReader bf = new BufferedReader(new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line = "";
				while ((line = bf.readLine()) != null) {
					sb.append(line);
				}
				String str = sb.toString();
				textPart.setText(str, "UTF-8");
				multipart.addBodyPart(textPart);
				message.setContent(str, "text/html");
			}

			Transport.send(message);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}