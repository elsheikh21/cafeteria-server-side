package com.ecs.co.cafeteria.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import com.ecs.co.cafeteria.orders.Order;

public class PrintJob {

	public static int printOrder(Order orderList) {
		PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
		PrintService svc = PrintServiceLookup.lookupDefaultPrintService();
		PrintRequestAttributeSet attrs = new HashPrintRequestAttributeSet();
		if (services.length > 0) {
			PrintService selection = ServiceUI.printDialog(null, 100, 100, services, svc, null, attrs);
			if (selection == null)
				return -1;
			else {
				try {
					DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
					InputStream inputStream = new FileInputStream("C:\\Users\\ahmede\\Desktop\\Test.txt");
					Doc doc = new SimpleDoc(inputStream, flavor, null);
					DocPrintJob job = selection.createPrintJob();
					job.print(doc, attrs);
					return 1;
				} catch (PrintException | FileNotFoundException exc) {
					return -1;
				}
			}
		}
		return -1;
	}
}
