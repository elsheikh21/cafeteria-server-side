package com.ecs.co.cafeteria.utilities;

import javax.servlet.ServletContext;

import com.authy.*;
import com.authy.api.*;

public class AuthyVerification {

	private static AuthyApiClient client;

	public static String registerUser(String user_email, String user_telephone, String user_country_code,
			ServletContext servletContext) {
		Config cfg = new Config(servletContext);
		String authy_api_key = cfg.getProperty("authy_api_key");
		client = new AuthyApiClient(authy_api_key);
		Users users = client.getUsers();
		User authyUser = users.createUser(user_email, user_telephone, user_country_code);
		if (authyUser.isOk()) {
			int authyUserId = authyUser.getId();
			client.getUsers().requestSms(authyUserId);
			return String.valueOf(authyUserId);
		} else {
			return String.valueOf(authyUser.getError());
		}
		// User user = users.createUser(user_email, user_telephone,
		// user_country_code);
		// if (user.isOk())
		// return String.valueOf(user.getId());
		// else
		// return String.valueOf(user.getError());
	}

	public static String verifyUser(String authy_ID, String user_token_input, ServletContext servletContext) {
		Config cfg = new Config(servletContext);
		String authy_api_key = cfg.getProperty("authy_api_key");
		client = new AuthyApiClient(authy_api_key);
		Tokens tokens = client.getTokens();
		Token verification = tokens.verify(Integer.parseInt(authy_ID), user_token_input);
		if (verification.isOk()) {
			return "Success";
		} else {
			return String.valueOf(verification.getError());
		}
	}

}
