package com.ecs.co.cafeteria.utilities;

import javax.servlet.ServletContext;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SendSMS {	
	public static String sendSMS(String to_Phone, String messageBody, ServletContext servletContext) {
		Config cfg = new Config(servletContext);
		String accountSID = cfg.getProperty("accountSID");
		String authToken = cfg.getProperty("authToken");
		String from_Phone = cfg.getProperty("from_Phone");
		Twilio.init(accountSID, authToken);
		Message message = Message.creator(new PhoneNumber(to_Phone), new PhoneNumber(from_Phone), messageBody).create();
		return message.getStatus().toString();
	}
}
