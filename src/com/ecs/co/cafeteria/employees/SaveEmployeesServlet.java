package com.ecs.co.cafeteria.employees;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/saveemployees")
public class SaveEmployeesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveEmployeesServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private EmployeesDAO employeesDAO;
	private DataSource ds;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			employeesDAO = new EmployeesDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception e) {
			response.getWriter().println("Get Operation failed with reason: " + e);
			LOGGER.error("Get Operation failed", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			// Access-Control-Allow-Origin
			String origin = request.getHeader("Origin");
			response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
			response.setHeader("Vary", "Origin");
			// Access-Control-Max-Age
			response.setHeader("Access-Control-Max-Age", "3600");
			// Access-Control-Allow-Credentials
			response.setHeader("Access-Control-Allow-Credentials", "true");
			// Access-Control-Allow-Methods
			response.setHeader("Access-Control-Allow-Methods", "POST, GET");
			// Access-Control-Allow-Headers
			response.setHeader("Access-Control-Allow-Headers",
					"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
			response.setContentType("application/json;charset=UTF-8");
			JSONObject jo = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					jo.put("status", doSave(request));
				} else {
					jo.put("status", "-100");
				}
			} else {
				jo.put("status", "-100");
			}
			response.getWriter().write(jo.toString());
		} catch (SQLException | JSONException e) {
			e.printStackTrace();
			LOGGER.error("Get Operation failed", e);
		}
	}

	private String doSave(HttpServletRequest request) throws SQLException, IOException, JSONException {
		String employeesJSON = request.getParameter("EmployeesJson");
		if (employeesJSON != null) {
			ArrayList<Employee> employees = new ArrayList<Employee>();
			JSONArray empJSONArr = new JSONArray(employeesJSON);
			JSONObject empJSONObj = new JSONObject();
			for (int i = 0; i < empJSONArr.length(); i++) {
				empJSONObj = empJSONArr.getJSONObject(i);
				Employee employee = new Employee();

				employee.setE_ID(empJSONObj.getInt("Employee ID"));
				employee.setE_First_Name(empJSONObj.getString("Employee First Name"));
				employee.setE_Last_Name(empJSONObj.getString("Employee Last Name"));
				employee.setE_Points(empJSONObj.getInt("Employee Points"));
				employee.setE_DepartmentID(empJSONObj.getInt("Development ID"));
				employee.setE_Attend("1".equals(String.valueOf(empJSONObj.getInt("Employee Attending"))));
				employee.setE_DepartmentName(empJSONObj.getString("Development Name"));

				employees.add(employee);
			}
			return employeesDAO.SaveEmployees(employees) ? "Success" : "failure";
		}
		return "failure";
	}
}

// *****************************************************************************************************
// In doSave method
// Employee employee = new Employee();
// FileInputStream fileStream = null;
// ArrayList<Employee> employees = new ArrayList<Employee>();
// try {
// // String FilePath = request.getParameter("FilePath");
// String FilePath = "C:\\Users\\ahmede\\Desktop\\testa.xlsx";
// File file = new File(FilePath);
// fileStream = new FileInputStream(file);
// XSSFWorkbook workbook = new XSSFWorkbook(fileStream);
// XSSFSheet sheet = workbook.getSheetAt(0);
// Iterator<Row> rowIterator = sheet.iterator();
// while (rowIterator.hasNext()) {
// Row row = rowIterator.next();
// Iterator<Cell> cellIterator = row.cellIterator();
// while (cellIterator.hasNext()) {
// Cell cell = cellIterator.next();
// cell.setCellType(Cell.CELL_TYPE_STRING);
// if (row.getRowNum() != 0) {
// switch (cell.getColumnIndex()) {
// case 0: // Employee ID, Integer
// employee.setE_ID(Integer.parseInt(row.getCell(0).getStringCellValue()));
// break;
// case 1: // Employee Name, String
// employee.setE_Name(row.getCell(1).getStringCellValue());
// break;
// case 2: // Employee Points, Integer
// employee.setE_Points(Integer.parseInt(row.getCell(2).getStringCellValue()));
// break;
// case 3: // Employee attendance, Boolean
// employee.setE_Attend(Boolean.parseBoolean(row.getCell(3).getStringCellValue()));
// break;
// case 4: // Employee Department Name, String
// employee.setE_DepartmentName(row.getCell(4).getStringCellValue());
// break;
// case 5: // Employee Department ID, Integer
// employee.setE_DepartmentID(Integer.parseInt(row.getCell(5).getStringCellValue()));
// break;
// }
// }
// }
// if (row.getRowNum() != 0)
// employees.add(employee);
// }
// return (employeesDAO.SaveEmployees(employees)) ? "Successfully uploaded."
// : "An error occurred, please re-upload the file.";
// } catch (Exception exc) {
// exc.printStackTrace();
// return "Please re-upload the file, an error occurred due to: " + exc;
// } finally {
// if (fileStream != null)
// fileStream.close();
// }
// *****************************************************************************************************
// In doPost method
// Part p = request.getPart("testa.xlsx");
// PrintWriter out = response.getWriter();
//
// String originalFilename = getFileName(p);
// out.println("Writing file with filename: " + originalFilename);
// p.write(originalFilename);
//
//
// *****************************************************************************************************
// A method of its own
// private String getFileName(Part part) {
// for (String cd : part.getHeader("content-disposition").split(";")) {
// if (cd.trim().startsWith("filename")) {
// return cd.substring(cd.indexOf('=') + 1).trim()
// .replace("\"", "");
// }
// }
// return null;
// }
