package com.ecs.co.cafeteria.employees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.ecs.co.cafeteria.points.ExtraPointsDAO;

public class EmployeesDAO {

	private DataSource dataSource;

	public EmployeesDAO(DataSource newDataSource) throws SQLException {
		setDataSource(newDataSource);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource newDataSource) throws SQLException {
		this.dataSource = newDataSource;
		checkTable();
	}

	private void checkTable() throws SQLException {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			if (!existsTable(connection)) {
				createTable(connection);
			}
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	private boolean existsTable(Connection conn) throws SQLException {
		DatabaseMetaData meta = conn.getMetaData();
		ResultSet rs = meta.getTables(null, null, "T_EMPLOYEES", null);
		while (rs.next()) {
			String name = rs.getString("TABLE_NAME");
			if (name.equals("T_EMPLOYEES")) {
				return true;
			}
		}
		return false;
	}

	private void createTable(Connection connection) throws SQLException {
		PreparedStatement pstmt = connection.prepareStatement(
				"CREATE COLUMN TABLE T_EMPLOYEES" + "(E_ID INTEGER PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, "
						+ "E_FIRST_NAME VARCHAR (255) NOT NULL, " + "E_LAST_NAME VARCHAR (255) NOT NULL, "
						+ "E_POINTS INTEGER NOT NULL, " + "E_ATTEND BOOLEAN NOT NULL, "
						+ "E_DEPARTMENTID INTEGER NOT NULL, " + "E_DEPARTMENTNAME VARCHAR(100) NOT NULL)");
		pstmt.executeUpdate();
	}

	public ArrayList<Employee> selectAllEmployees() throws SQLException {
		checkTable();
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_EMPLOYEES ORDER BY E_ID");
			ResultSet rs = pstmt.executeQuery();
			ArrayList<Employee> list = new ArrayList<Employee>();
			if (rs.next()) {
				do {
					Employee employee = new Employee();
					employee.setE_ID(rs.getInt("E_ID"));
					employee.setE_First_Name(rs.getString("E_FIRST_NAME"));
					employee.setE_Last_Name(rs.getString("E_LAST_NAME"));
					employee.setE_Points(rs.getInt("E_POINTS"));
					employee.setE_Attend(rs.getBoolean("E_ATTEND"));
					employee.setE_DepartmentID(rs.getInt("E_DEPARTMENTID"));
					employee.setE_DepartmentName(rs.getString("E_DEPARTMENTNAME"));
					list.add(employee);
				} while (rs.next());
			}
			return list;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public Employee searchEmployee(int employeeID) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		Employee employee = new Employee();
		try {
			pstmt = connection.prepareStatement("SELECT * FROM T_EMPLOYEES WHERE E_ID = ?");
			pstmt.setInt(1, employeeID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				employee.setE_ID(rs.getInt("E_ID"));
				employee.setE_First_Name(rs.getString("E_FIRST_NAME"));
				employee.setE_Last_Name(rs.getString("E_LAST_NAME"));
				employee.setE_Points(rs.getInt("E_POINTS"));
				employee.setE_Attend(rs.getBoolean("E_ATTEND"));
				employee.setE_DepartmentID(rs.getInt("E_DEPARTMENTID"));
				employee.setE_DepartmentName(rs.getString("E_DEPARTMENTNAME"));
			}
			return employee;
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public boolean SaveEmployees(ArrayList<Employee> employees) throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			int executeBatchEvery = 0;
			int[] updatesNumber = new int[employees.size()];
			connection.setAutoCommit(false);
			if (DeleteAllEntries()) {
				checkTable();
				pstmt = connection.prepareStatement("INSERT INTO T_EMPLOYEES "
						+ "(E_FIRST_NAME, E_LAST_NAME, E_POINTS, E_ATTEND, E_DEPARTMENTNAME, E_DEPARTMENTID)"
						+ " VALUES (?, ?, ?, ?, ?, ?)");
				for (Employee employee : employees) {
					pstmt.setString(1, employee.getE_First_Name());
					pstmt.setString(2, employee.getE_Last_Name());
					pstmt.setInt(3, employee.getE_Points());
					pstmt.setBoolean(4, employee.getE_Attend());
					pstmt.setString(5, employee.getE_DepartmentName());
					pstmt.setInt(6, employee.getE_DepartmentID());
					pstmt.addBatch();
					executeBatchEvery++;
					if (executeBatchEvery % 1000 == 0 || executeBatchEvery == employees.size()) {
						updatesNumber = pstmt.executeBatch();
						for (int updateReturnCodes : updatesNumber) {
							if (updateReturnCodes <= 0) {
								connection.rollback();
								return false;
							}
						}
					}
				}
				return true;
			}
			return false;
		} finally {
			if (pstmt != null) {
				pstmt.clearBatch();
				pstmt.close();
			}
			if (connection != null) {
				connection.commit();
				connection.close();
			}
		}
	}

	// int captainUser
	public boolean AddPoints(int employeeID) throws SQLException {
		boolean isAdded = false;
		Connection connection = dataSource.getConnection();
		try {
			PreparedStatement pstmt = connection.prepareStatement("SELECT * FROM T_EMPLOYEES WHERE E_ID = ?");
			pstmt.setInt(1, employeeID);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				boolean employeeAttending = rs.getBoolean("E_ATTEND");
				if (!employeeAttending)
					return false;
				int employeePoints = rs.getInt("E_POINTS");
				int extraPoints = fetchExtraPoints();
				pstmt = connection.prepareStatement("UPDATE T_EMPLOYEES SET E_POINTS = ? WHERE E_ID = ?");
				pstmt.setInt(1, employeePoints + extraPoints);
				pstmt.setInt(2, employeeID);
				if (pstmt.executeUpdate() > 0) {
					isAdded = true;
					ExtraPointsDAO extraPointsDAO = new ExtraPointsDAO(dataSource);
					boolean AppendedToTable = extraPointsDAO.AddExtra(employeeID, 1);
					if (!(isAdded && AppendedToTable)) {
						connection.rollback();
						isAdded = false;
					}
				}
			}
		} finally {
			if (connection != null)
				connection.close();
		}
		return isAdded;
	}

	public boolean deleteEmployees(String[] employeeIDs) throws SQLException {
		int employeeIDsArrLength = employeeIDs.length;
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("DELETE FROM T_EMPLOYEES WHERE E_ID IN (?)");
			String employeeIDsStr = Arrays.toString(employeeIDs).replace("[", "").replace("]", "");
			pstmt.setString(1, employeeIDsStr);
			return pstmt.executeUpdate() == employeeIDsArrLength ? true : false;
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (connection != null)
				connection.close();
		}
	}

	private boolean DeleteAllEntries() throws SQLException {
		Connection connection = dataSource.getConnection();
		if (existsTable(connection)) {
			PreparedStatement pstmt = null;
			try {
				pstmt = connection.prepareStatement("DELETE FROM T_EMPLOYEES");
				return pstmt.executeUpdate() >= 0 ? true : false;
			} finally {
				if (pstmt != null)
					pstmt.close();
				if (connection != null)
					connection.close();
			}
		}
		return false;
	}

	private int fetchExtraPoints() throws SQLException {
		Connection connection = dataSource.getConnection();
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("SELECT P_EXTRA_POINTS FROM T_POINTS");
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
				return rs.getInt("P_EXTRA_POINTS");
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		return 0;
	}

}