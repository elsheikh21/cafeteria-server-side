package com.ecs.co.cafeteria.employees;

public class Employee {

	private int E_ID;
	private String E_First_Name;
	private String E_Last_Name;
	private int E_Points;
	private boolean E_Attend;
	private int E_DepartmentID;
	private String E_DepartmentName;

	public int getE_ID() {
		return E_ID;
	}

	public void setE_ID(int e_ID) {
		E_ID = e_ID;
	}

	public String getE_First_Name() {
		return E_First_Name;
	}

	public void setE_First_Name(String e_Name) {
		E_First_Name = e_Name;
	}

	public String getE_Last_Name() {
		return E_Last_Name;
	}

	public void setE_Last_Name(String e_Last_Name) {
		E_Last_Name = e_Last_Name;
	}

	public int getE_Points() {
		return E_Points;
	}

	public void setE_Points(int e_Points) {
		E_Points = e_Points;
	}

	public boolean getE_Attend() {
		return E_Attend;
	}

	public void setE_Attend(boolean e_Attend) {
		E_Attend = e_Attend;
	}

	public int getE_DepartmentID() {
		return E_DepartmentID;
	}

	public void setE_DepartmentID(int e_DepartmentID) {
		E_DepartmentID = e_DepartmentID;
	}

	public String getE_DepartmentName() {
		return E_DepartmentName;
	}

	public void setE_DepartmentName(String e_DepartmentName) {
		E_DepartmentName = e_DepartmentName;
	}

	@Override
	public String toString() {
		return "Employee [E_ID=" + E_ID + ", E_First_Name=" + E_First_Name + ", E_Last_Name=" + E_Last_Name
				+ ", E_Points=" + E_Points + ", E_Attend=" + E_Attend + ", E_DepartmentID=" + E_DepartmentID
				+ ", E_DepartmentName=" + E_DepartmentName + "]";
	}
	
	
}
