package com.ecs.co.cafeteria.employees;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/selectemployee")
public class SelectEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SelectEmployeeServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private EmployeesDAO employeesDAO;
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			employeesDAO = new EmployeesDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception exc) {
			response.getWriter().println("Get Operation failed with reason: " + exc);
			LOGGER.error("Get Operation failed", exc);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Access-Control-Allow-Origin
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
		response.setHeader("Vary", "Origin");
		// Access-Control-Max-Age
		response.setHeader("Access-Control-Max-Age", "3600");
		// Access-Control-Allow-Credentials
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// Access-Control-Allow-Methods
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		// Access-Control-Allow-Headers
		response.setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
		response.setContentType("application/json;charset=UTF-8");
		JSONObject jo = new JSONObject();
		JSONObject empObj = new JSONObject();
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					Employee employee = doSelect(request);
					jo.put("id", employee.getE_ID());
					jo.put("first_name", employee.getE_First_Name());
					jo.put("last_name", employee.getE_Last_Name());
					jo.put("points", employee.getE_Points());
					jo.put("attending", employee.getE_Attend());
					jo.put("department_id", employee.getE_DepartmentID());
					jo.put("department_name", employee.getE_DepartmentName());

					empObj.put("Employee", jo);
				} else {
					empObj.put("status", "-100");
				}
			} else {
				empObj.put("status", "-100");
			}
			response.getWriter().write(empObj.toString());
		} catch (SQLException | JSONException e) {
			response.getWriter().println("Post Operation failed with reason: " + e.getMessage());
			LOGGER.error("Operation failed", e);
		}
	}

	private Employee doSelect(HttpServletRequest request) throws NumberFormatException, SQLException {
		String employeeID = request.getParameter("employeeID");
		if (employeeID != null) {
			if (employeeID.trim().length() > 0 && employeeID.matches("[0-9]+"))
				return employeesDAO.searchEmployee(Integer.parseInt(employeeID));
		}
		return null;
	}
}
