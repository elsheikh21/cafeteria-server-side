package beverages;

public class Beverages {

	private int B_ID;
	private String B_Category;
	private int B_Points;
	private int B_Quantity;
	private String B_Category_ID;
	private int B_Demand;

	public int getB_ID() {
		return B_ID;
	}

	public void setB_ID(int b_ID) {
		B_ID = b_ID;
	}

	public String getB_Category() {
		return B_Category;
	}

	public void setB_Category(String b_Category) {
		B_Category = b_Category;
	}

	public int getB_Points() {
		return B_Points;
	}

	public void setB_Points(int b_Points) {
		B_Points = b_Points;
	}

	public int getB_Quantity() {
		return B_Quantity;
	}

	public void setB_Quantity(int b_Quantity) {
		B_Quantity = b_Quantity;
	}

	public String getB_Category_ID() {
		return B_Category_ID;
	}

	public void setB_Category_ID(String b_Category_ID) {
		B_Category_ID = b_Category_ID;
	}

	public int getB_Demand() {
		return B_Demand;
	}

	public void setB_Demand(int b_Demand) {
		B_Demand = b_Demand;
	}
}
