package beverages;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/SaveBeverages")
public class SaveBeveragesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveBeveragesServlet.class);
	private final List<String> allowedOrigins = Arrays.asList(
			"https://webidetesting8919616-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8919617-p1942851539trial.dispatcher.hanatrial.ondemand.com",
			"https://webidetesting8917693-p1942851539trial.dispatcher.hanatrial.ondemand.com");
	private BeveragesDAO beveragesDAO;
	private DataSource ds;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");
			beveragesDAO = new BeveragesDAO(ds);
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (Exception exc) {
			response.getWriter().println("Get Operation failed with reason: " + exc);
			LOGGER.error("Get Operation failed", exc);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			// Access-Control-Allow-Origin
			String origin = request.getHeader("Origin");
			response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
			response.setHeader("Vary", "Origin");
			// Access-Control-Max-Age
			response.setHeader("Access-Control-Max-Age", "3600");
			// Access-Control-Allow-Credentials
			response.setHeader("Access-Control-Allow-Credentials", "true");
			// Access-Control-Allow-Methods
			response.setHeader("Access-Control-Allow-Methods", "POST, GET");
			// Access-Control-Allow-Headers
			response.setHeader("Access-Control-Allow-Headers",
					"Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");
			response.setContentType("application/json;charset=UTF-8");
			JSONObject jo = new JSONObject();
			HttpSession session = request.getSession(false);
			if (session != null) {
				String user = (String) session.getServletContext().getAttribute("User");
				if (user != null) {
					jo.put("status", doSave(request));
				} else {
					jo.put("status", "-100");
				}
			} else {
				jo.put("status", "-100");
			}
			response.getWriter().write(jo.toString());
		} catch (SQLException | JSONException e) {
			e.printStackTrace();
			LOGGER.error("Get Operation failed", e);
		}
	}

	private String doSave(HttpServletRequest request) throws SQLException, IOException, JSONException {
		String beveragesJSON = request.getParameter("BeveragesJson");
		if (beveragesJSON != null) {
			ArrayList<Beverages> beverages = new ArrayList<Beverages>();
			JSONArray beveragesJSONArr = new JSONArray(beveragesJSON);
			JSONObject beveragesJSONObj = new JSONObject();
			for (int i = 0; i < beveragesJSONArr.length(); i++) {
				beveragesJSONObj = beveragesJSONArr.getJSONObject(i);
				Beverages beverage = new Beverages();
				beverage.setB_Category(beveragesJSONObj.getString("Categories"));
				beverage.setB_Points(beveragesJSONObj.getInt("Points"));
				beverage.setB_Category_ID(beveragesJSONObj.getString("categoryID"));
				beverages.add(beverage);
			}
			return beveragesDAO.SaveBeverages(beverages) ? "Success" : "failure";
		}
		return "failure";
	}
}